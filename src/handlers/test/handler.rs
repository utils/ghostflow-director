// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::cmp::Ordering;
use std::error::Error;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};

use async_trait::async_trait;
use json_job_dispatch::{Director, Handler, HandlerCore, JobResult};
use log::error;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde_json::Value;
use thiserror::Error;

use crate::config::Host;
use crate::handlers::common::handlers::*;
use crate::handlers::common::jobs::{BatchBranchJob, ClearTestRefs, TagStage, UpdateFollowRefs};
use crate::handlers::test::hooks::*;

/// The handler for test service jobs.
pub struct TestHandler {
    /// The host block for this handler.
    host: Arc<RwLock<Host>>,
}

#[derive(Deserialize, Debug)]
struct CloneProjects;

#[derive(Deserialize, Debug)]
struct ResetFailedProjects;

#[derive(Deserialize, Debug)]
struct Delay {
    path: String,
    millis: u64,
}

#[derive(Deserialize, Debug)]
struct Signal {
    path: String,
}

const HOST_LOCK_POISONED: &str = "host lock poisoned";

#[derive(Debug, Error)]
enum DelayError {
    #[error("delay limit reached: {}", path.display())]
    LimitReached { path: PathBuf },
}

impl DelayError {
    fn limit_reached(path: PathBuf) -> Self {
        DelayError::LimitReached {
            path,
        }
    }
}

impl TestHandler {
    /// Create a new handler.
    pub fn new(host: Host) -> Self {
        TestHandler {
            host: Arc::new(RwLock::new(host)),
        }
    }

    /// Parse an object into a type.
    fn parse_object<F, T>(object: &Value, callback: F) -> JobResult
    where
        T: DeserializeOwned,
        F: Fn(T) -> JobResult,
    {
        match serde_json::from_value::<T>(object.clone()) {
            Ok(hook) => callback(hook),
            Err(err) => {
                error!("failed to parse service webhook: {:?}", err);
                JobResult::fail(err)
            },
        }
    }

    /// Handle a job.
    fn handle_kind(
        host: Arc<RwLock<Host>>,
        kind: String,
        object: Value,
        can_defer: bool,
    ) -> JobResult {
        match kind.as_ref() {
            "merge_request" => {
                Self::parse_object(&object, |hook: MergeRequestHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    if hook.action == MergeRequestAction::Comment {
                        handle_merge_request_note(&object, &host, &hook.into(), can_defer)
                    } else {
                        handle_merge_request_update(&object, &host, &hook.into())
                    }
                })
            },
            "push" => {
                Self::parse_object(&object, |hook: PushHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_push(&object, &host, &hook.into())
                })
            },
            "project" => {
                Self::parse_object(&object, |hook: ProjectHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    let repo = hook.project.into();
                    handle_project_creation(&object, &host, &repo)
                })
            },
            "clear_test_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<ClearTestRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_clear_test_refs(&object, &host, data)
                })
            },
            "tag_stage" => {
                Self::parse_object(&object, |data: BatchBranchJob<TagStage>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_stage_tag(&object, &host, data)
                })
            },
            "update_follow_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<UpdateFollowRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_update_follow_refs(&object, &host, data)
                })
            },
            "clone_projects" => {
                Self::parse_object(&object, |_: CloneProjects| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    host.project_names().for_each(|name| {
                        host.project(name);
                    });
                    JobResult::Accept
                })
            },
            "reset_failed_projects" => {
                Self::parse_object(&object, |_: ResetFailedProjects| {
                    host.write()
                        .expect(HOST_LOCK_POISONED)
                        .reset_failed_projects();
                    JobResult::Accept
                })
            },
            "delay" => {
                Self::parse_object(&object, |data: Delay| {
                    let path: &Path = data.path.as_ref();
                    let limit = Duration::from_millis(data.millis);
                    let start = Instant::now();
                    while !path.exists() {
                        if Instant::now().duration_since(start) > limit {
                            error!("delay limit for {} reached", path.display());
                            return JobResult::fail(DelayError::limit_reached(path.into()));
                        }
                    }
                    JobResult::Accept
                })
            },
            "signal" => {
                Self::parse_object(&object, |data: Signal| {
                    match File::create(data.path) {
                        Ok(_) => JobResult::Accept,
                        Err(err) => JobResult::fail(err),
                    }
                })
            },
            _ => JobResult::reject(format!("unhandled kind: {}", kind)),
        }
    }
}

impl HandlerCore for TestHandler {
    fn add_to_director<'a>(
        &'a self,
        director: &mut Director<'a>,
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        let mut add_handler = |kind| director.add_handler(kind, self);

        add_handler("merge_request")?;
        add_handler("push")?;
        add_handler("project")?;
        add_handler("membership")?;

        add_handler("clear_test_refs")?;
        add_handler("tag_stage")?;
        add_handler("update_follow_refs")?;

        add_handler("clone_projects")?;
        add_handler("reset_failed_projects")?;

        add_handler("delay")?;
        add_handler("signal")?;

        Ok(())
    }
}

const DEFAULT_RETRY_LIMIT: usize = 5;

#[async_trait]
impl Handler for TestHandler {
    async fn handle(
        &self,
        kind: &str,
        object: &Value,
        count: usize,
    ) -> Result<JobResult, Box<dyn Error + Send + Sync>> {
        let can_defer = match count.cmp(&DEFAULT_RETRY_LIMIT) {
            Ordering::Greater => {
                return Ok(JobResult::reject(format!(
                    "retry limit ({}) reached for {}",
                    count, kind,
                )))
            },
            Ordering::Equal => false,
            Ordering::Less => true,
        };

        let host = self.host.clone();
        let kind = kind.into();
        let object = object.clone();

        tokio::task::spawn_blocking(move || Self::handle_kind(host, kind, object, can_defer))
            .await
            .map_err(|err| Box::new(err) as Box<dyn Error + Send + Sync>)
    }
}

mod tests {
    use std::fs::{self, File};
    use std::thread;

    use serde_json::json;

    use crate::handlers::test::*;

    #[test]
    fn test_defer_clone() {
        let mut service = TestService::new(
            "test_defer_clone",
            [
                Action::new_project("ghostflow", "example"),
                Action::new_project("ghostflow", "defer"),
                // Ensure the `example` project is cloned.
                Action::push(
                    "ghostflow",
                    "example",
                    "master",
                    "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51",
                ),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
            "ghostflow/defer": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::Ignore;

        service.launch_timeout(config, end).unwrap();

        let project_subdir = "projects/ghostflow/example.git";
        let missing_subdir = "projects/ghostflow/missing.git";
        let refpath = "refs/heads/master";
        let clone_indicator = service
            .root()
            .join("director")
            .join(project_subdir)
            .join(refpath);
        let missing_indicator = service
            .root()
            .join("director")
            .join(missing_subdir)
            .join(refpath);

        assert!(clone_indicator.exists());
        assert!(!missing_indicator.exists());
    }

    enum ShouldReset {
        Yes,
        No,
    }

    fn test_init_failed(name: &str, reset: ShouldReset) {
        let mut service = TestService::new(
            name,
            [
                Action::new_project("ghostflow", "example"),
                Action::new_project("ghostflow", "missing"),
                Action::delay_director("move-missing", 500),
                Action::CloneProjects,
                match reset {
                    ShouldReset::Yes => Action::ResetFailedProjects,
                    ShouldReset::No => Action::Ignore,
                },
                Action::signal("clone-done"),
                Action::delay_director("restore-missing", 500),
                Action::CloneProjects,
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
            "ghostflow/missing": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::Ignore;

        let delay_dir = service.root().join("delay");
        let move_missing_path = delay_dir.join("move-missing");
        let restore_missing_path = delay_dir.join("restore-missing");

        let missing_subdir = "projects/ghostflow/missing.git";

        let missing_dir = service.root().join(missing_subdir);
        let missing_temp_dir = service.root().join(format!("{}.bak", missing_subdir));

        let signal = service.root().join("signal").join("clone-done");

        let temporary_failure = thread::spawn(move || {
            fs::rename(&missing_dir, &missing_temp_dir)
                .map_err(|_| "failed to restore the repository")?;

            File::create(move_missing_path).map_err(|_| "failed to create the delay file")?;

            // Wait for cloning to complete.
            loop {
                if signal.exists() {
                    break;
                }
            }

            fs::rename(missing_temp_dir, missing_dir)
                .map_err(|_| "failed to restore the repository")?;

            File::create(restore_missing_path).map_err(|_| "failed to create the delay file")
        });

        service.launch_timeout(config, end).unwrap();
        temporary_failure.join().unwrap().unwrap();

        let refpath = "refs/heads/master";
        let missing_indicator = service
            .root()
            .join("director")
            .join(missing_subdir)
            .join(refpath);
        assert!(match reset {
            ShouldReset::Yes => missing_indicator.exists(),
            ShouldReset::No => !missing_indicator.exists(),
        });
    }

    #[test]
    fn test_failed_projects_stay_failed() {
        test_init_failed("test_failed_projects_stay_failed", ShouldReset::No)
    }

    #[test]
    fn test_reset_failed_projects() {
        test_init_failed("test_reset_failed_projects", ShouldReset::Yes)
    }
}
