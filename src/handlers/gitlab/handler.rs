// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::cmp::Ordering;
use std::error::Error;
use std::sync::{Arc, RwLock};

use async_trait::async_trait;
use either::Either;
use ghostflow::host::HostingServiceError;
use ghostflow_gitlab::gitlab::{Gitlab, GitlabError};
use ghostflow_gitlab::GitlabService;
use json_job_dispatch::{Director, Handler, HandlerCore, JobResult};
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde_json::Value;
use thiserror::Error;

use crate::actions::merge_requests::Info;
use crate::config::defaults;
use crate::config::Host;
use crate::handlers::common::handlers::*;
use crate::handlers::common::jobs::{
    BatchBranchJob, ClearTestRefs, ResetFailedProjects, TagStage, UpdateFollowRefs,
};
use crate::handlers::gitlab::hooks::{
    GroupMemberSystemHook, MergeRequestHook, NoteHook, NoteType, ProjectMemberSystemHook,
    ProjectSystemHook, PushHook,
};
use crate::handlers::gitlab::support;
use crate::handlers::gitlab::traits::*;
use crate::handlers::HostHandler;

#[derive(Debug, Error)]
pub enum GitlabHostError {
    #[error("failed to deserialize GitLab secrets for host {}: {}", host, source)]
    DeserializeSecrets {
        host: String,
        #[source]
        source: serde_json::Error,
    },
    #[error("failed to construct client for host {}: {}", host, source)]
    ConstructClient {
        host: String,
        #[source]
        source: GitlabError,
    },
    #[error("failed to construct service for host {}: {}", host, source)]
    ConstructService {
        host: String,
        #[source]
        source: HostingServiceError,
    },
}

impl GitlabHostError {
    fn deserialize_secrets(host: String, source: serde_json::Error) -> Self {
        GitlabHostError::DeserializeSecrets {
            host,
            source,
        }
    }

    fn construct_client(host: String, source: GitlabError) -> Self {
        GitlabHostError::ConstructClient {
            host,
            source,
        }
    }

    fn construct_service(host: String, source: HostingServiceError) -> Self {
        GitlabHostError::ConstructService {
            host,
            source,
        }
    }
}

#[derive(Debug, Deserialize)]
struct GitlabSecrets {
    token: String,
    #[serde(default = "defaults::default_true")]
    use_ssl: bool,
}

/// Connect to a Gitlab from its configuration block.
pub fn host_handler(
    url: &Option<String>,
    secrets: &Value,
    name: String,
) -> Result<HostHandler, GitlabHostError> {
    let host = url.as_ref().map_or("gitlab.com", |u| u.as_str());
    let secrets: GitlabSecrets = serde_json::from_value(secrets.clone())
        .map_err(|err| GitlabHostError::deserialize_secrets(host.into(), err))?;

    let ctor = if secrets.use_ssl {
        Gitlab::new
    } else {
        Gitlab::new_insecure
    };

    let gitlab = ctor(host, secrets.token)
        .map_err(|err| GitlabHostError::construct_client(host.into(), err))?;
    let service = GitlabService::new(gitlab.clone())
        .map(Arc::new)
        .map_err(|err| GitlabHostError::construct_service(host.into(), err))?;

    Ok(HostHandler {
        service,
        handler: Box::new(|host| {
            Box::new(GitlabHandler::new(gitlab, host, name)) as Box<dyn Handler>
        }),
    })
}

/// The handler for Gitlab events.
struct GitlabHandler {
    /// Handle to a private client.
    gitlab: Arc<Gitlab>,
    /// The host block for this handler.
    host: Arc<RwLock<Host>>,
    /// The name to use for this handler.
    name: String,
}

const HOST_LOCK_POISONED: &str = "host lock poisoned";

impl GitlabHandler {
    /// Create a new handler.
    fn new(gitlab: Gitlab, host: Host, name: String) -> Self {
        GitlabHandler {
            gitlab: Arc::new(gitlab),
            host: Arc::new(RwLock::new(host)),
            name,
        }
    }

    /// Verify that the kind is valid.
    fn verify_kind<'a>(&self, kind: &'a str) -> Either<&'a str, JobResult> {
        let mut split = kind.split(':');

        if let Some(level) = split.next() {
            if level != self.name {
                return Either::Right(JobResult::reject(format!("handler mismatch: {}", level)));
            }
        } else {
            return Either::Right(JobResult::reject("handler mismatch"));
        }

        if let Some(kind) = split.next() {
            Either::Left(kind)
        } else {
            Either::Right(JobResult::reject("missing kind"))
        }
    }

    /// Parse an object into a type.
    fn parse_object<F, T>(object: &Value, callback: F) -> JobResult
    where
        T: DeserializeOwned,
        F: Fn(T) -> JobResult,
    {
        match serde_json::from_value::<T>(object.clone()) {
            Ok(hook) => callback(hook),
            Err(err) => JobResult::fail(err),
        }
    }

    /// Handle a job.
    fn handle_kind(
        host: Arc<RwLock<Host>>,
        gitlab: Arc<Gitlab>,
        kind: String,
        object: Value,
        can_defer: bool,
    ) -> JobResult {
        match kind.as_ref() {
            "merge_request" => {
                Self::parse_object(&object, |hook: MergeRequestHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    let info = GitlabMergeRequestInfo::from_web_hook(
                        host.service.as_ref(),
                        &hook.object_attributes,
                    );

                    // XXX(gitlab): Only needed until something like this feature has landed:
                    // https://gitlab.com/gitlab-org/gitlab/-/issues/25889
                    if let Ok(mr) = info.as_ref() {
                        if mr.is_open {
                            Self::unprotect_source_branch(&host, &gitlab, mr);
                        }
                    }

                    info.map(|mr| handle_merge_request_update(&object, &host, &mr))
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "note" => {
                Self::parse_object(&object, |hook: NoteHook| {
                    let note_type = hook.object_attributes.noteable_type;
                    if let NoteType::MergeRequest = note_type {
                        let host = host.read().expect(HOST_LOCK_POISONED);
                        GitlabMergeRequestNoteInfo::from_web_hook(host.service.as_ref(), &hook)
                            .map(|note| handle_merge_request_note(&object, &host, &note, can_defer))
                            .unwrap_or_else(JobResult::fail)
                    } else {
                        JobResult::reject(format!("unhandled noteable type: {:?}", note_type))
                    }
                })
            },
            "push" => {
                Self::parse_object(&object, |hook: PushHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    GitlabPushInfo::from_web_hook(host.service.as_ref(), hook)
                        .map(|push| handle_push(&object, &host, &push))
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "project_create" => {
                Self::parse_object(&object, |hook: ProjectSystemHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    host.service
                        .repo(&hook.path_with_namespace)
                        .map(|project| handle_project_creation(&object, &host, &project))
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "user_add_to_team" | "user_remove_from_team" => {
                Self::parse_object(&object, |hook: ProjectMemberSystemHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_project_membership_refresh(
                        &object,
                        &host,
                        &hook.project_path_with_namespace,
                    )
                })
            },
            "user_add_to_group" | "user_remove_from_group" => {
                Self::parse_object(&object, |hook: GroupMemberSystemHook| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_group_membership_refresh(&object, &host, &hook.group_name)
                })
            },
            "clear_test_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<ClearTestRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_clear_test_refs(&object, &host, data)
                })
            },
            "tag_stage" => {
                Self::parse_object(&object, |data: BatchBranchJob<TagStage>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_stage_tag(&object, &host, data)
                })
            },
            "update_follow_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<UpdateFollowRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_update_follow_refs(&object, &host, data)
                })
            },
            "ensure_project_state" => {
                Self::parse_object(&object, |ensure: support::EnsureProjectState| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    ensure.check_all_projects(&gitlab, &host)
                })
            },
            "reset_failed_projects" => {
                Self::parse_object(&object, |_: ResetFailedProjects| {
                    host.write()
                        .expect(HOST_LOCK_POISONED)
                        .reset_failed_projects();
                    JobResult::Accept
                })
            },
            _ => JobResult::reject(format!("unhandled kind: {}", kind)),
        }
    }

    fn unprotect_source_branch(host: &Host, gitlab: &Gitlab, mr: &Info) {
        let project =
            if let Ok(project) = utils::get_project(host, &mr.merge_request.target_repo.name) {
                project
            } else {
                return;
            };

        if !project.unprotect_source_branches() {
            return;
        }

        support::unprotect_source_branch(gitlab, host, mr)
    }
}

impl HandlerCore for GitlabHandler {
    fn add_to_director<'a>(
        &'a self,
        director: &mut Director<'a>,
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        let mut add_handler = |kind| director.add_handler(format!("{}:{}", self.name, kind), self);

        add_handler("merge_request")?;
        add_handler("note")?;
        add_handler("push")?;
        add_handler("project_create")?;
        add_handler("user_add_to_team")?;
        add_handler("user_remove_from_team")?;
        add_handler("user_add_to_group")?;
        add_handler("user_remove_from_group")?;

        add_handler("clear_test_refs")?;
        add_handler("tag_stage")?;
        add_handler("update_follow_refs")?;
        add_handler("ensure_project_state")?;

        add_handler("reset_failed_projects")?;

        Ok(())
    }
}

const DEFAULT_RETRY_LIMIT: usize = 5;

#[async_trait]
impl Handler for GitlabHandler {
    async fn handle(
        &self,
        kind: &str,
        object: &Value,
        count: usize,
    ) -> Result<JobResult, Box<dyn Error + Send + Sync>> {
        let can_defer = match count.cmp(&DEFAULT_RETRY_LIMIT) {
            Ordering::Greater => {
                return Ok(JobResult::reject(format!(
                    "retry limit ({}) reached for {}",
                    count, kind,
                )))
            },
            Ordering::Equal => false,
            Ordering::Less => true,
        };

        let kind = match self.verify_kind(kind) {
            Either::Left(kind) => kind,
            Either::Right(res) => return Ok(res),
        };

        let host = self.host.clone();
        let gitlab = self.gitlab.clone();
        let kind = kind.into();
        let object = object.clone();

        tokio::task::spawn_blocking(move || {
            Self::handle_kind(host, gitlab, kind, object, can_defer)
        })
        .await
        .map_err(|err| Box::new(err) as Box<dyn Error + Send + Sync>)
    }
}
