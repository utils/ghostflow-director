// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use ghostflow_gitlab::gitlab::api::{self, Query};
use ghostflow_gitlab::gitlab::Gitlab;
use json_job_dispatch::JobResult;
use log::{error, warn};
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use serde::Deserialize;
use thiserror::Error;

use crate::actions::merge_requests::Info;
use crate::config::Host;
use crate::ghostflow_ext::DirectorHostingService;

#[derive(Debug, Error)]
enum UnprotectError {
    #[error("gitlab error: {}", source)]
    Gitlab {
        #[from]
        source: api::ApiError<<Gitlab as api::RestClient>::Error>,
    },
}

enum Unprotect {
    Unnecessary,
    Unknown,
    Performed,
}

pub fn unprotect_source_branch(gitlab: &Gitlab, host: &Host, mr: &Info) {
    static UNPROTECTED_SOURCE_BRANCH_COMMENT: &str =
        "The source branch has been detected as being protected. Various steps in this project's \
         workflow require rewriting history (including, but not limited to, any code \
         reformatting, commit squashing, and commit message clarifications). Your source branch \
         has been automatically unprotected, but in the future, please try to use topic branches \
         for development.";

    match unprotect(gitlab, mr) {
        Ok(Unprotect::Unnecessary) => (),
        Ok(Unprotect::Performed) => {
            if let Err(err) = host
                .service
                .post_mr_comment(&mr.merge_request, UNPROTECTED_SOURCE_BRANCH_COMMENT)
            {
                error!(
                    target: "ghostflow-director/gitlab",
                    "failed to post source branch unprotection message to {}: {}",
                    mr.merge_request.url,
                    err,
                );
            }
        },
        Ok(Unprotect::Unknown) => {
            warn!(
                target: "ghostflow-director/gitlab",
                "could not determine whether to unprotect the source branch for {}",
                mr.merge_request.url,
            );
        },
        Err(err) => {
            error!(
                target: "ghostflow-director/gitlab",
                "failed to unprotect the source branch for {}: {}",
                mr.merge_request.url,
                err,
            );
        },
    }
}

#[derive(Debug, Deserialize)]
struct Branch {
    protected: Option<bool>,
}

fn unprotect(gitlab: &Gitlab, mr: &Info) -> Result<Unprotect, UnprotectError> {
    let repo = mr
        .merge_request
        .source_repo
        .as_ref()
        .unwrap_or(&mr.merge_request.target_repo);
    let endpoint = api::projects::repository::branches::Branch::builder()
        .project(repo.name.as_str())
        .branch(mr.merge_request.source_branch.as_str())
        .build()
        .unwrap();
    let branch: Branch = endpoint.query(gitlab)?;

    Ok(match branch.protected {
        Some(true) => {
            let endpoint = api::projects::protected_branches::UnprotectBranch::builder()
                .project(repo.name.as_str())
                .name(mr.merge_request.source_branch.as_str())
                .build()
                .unwrap();
            let endpoint = api::ignore(endpoint);
            endpoint.query(gitlab)?;

            Unprotect::Performed
        },
        Some(false) => Unprotect::Unnecessary,
        None => Unprotect::Unknown,
    })
}

#[derive(Debug, Deserialize)]
struct Project {
    id: u64,
    path_with_namespace: String,
}

#[derive(Debug, Deserialize)]
struct Hook {
    id: u64,
    url: String,

    // XXX: Keep in sync with `src/handlers/gitlab/ghostflow_ext.rs@add_hook`.
    issues_events: bool,
    job_events: bool,
    merge_requests_events: bool,
    note_events: bool,
    pipeline_events: bool,
    push_events: bool,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct EnsureProjectState {}

impl EnsureProjectState {
    // XXX: Keep in sync with `src/handlers/common/handlers/projects.rs@handle_project_creation`.
    pub fn check_all_projects(&self, gitlab: &Gitlab, host: &Host) -> JobResult {
        use ghostflow_gitlab::gitlab::api::Pageable;

        let webhook_url = host.webhook_url.as_ref();
        let global_user = host.global_user.as_ref();

        // First, check up front as to whether there is any work to do or not.
        if webhook_url.is_none() && global_user.is_none() {
            return JobResult::reject("nothing to do");
        }

        // Start building up our project query.
        let endpoint = api::projects::Projects::builder()
            // This is the only way keyset pagination is supported right now.
            .order_by(api::projects::ProjectOrderBy::Id)
            // Sort ascending to ensure that new projects show up at the end.
            .sort(api::common::SortOrder::Ascending)
            // We don't need much information here.
            .simple(true)
            .build()
            .unwrap();

        // Check that it supports keyset pagination. This helps ensure the stability of the
        // returned project list.
        if !endpoint.use_keyset_pagination() {
            error!(
                target: "ghostflow-director/gitlab",
                "the 'all projects' query does not support keyset pagination; projects may be \
                 lost in races related to pagination races",
            );
        }

        // Query *all* projects.
        let endpoint = api::paged(endpoint, api::Pagination::All);
        let projects: Vec<Project> = match endpoint.query(gitlab) {
            Ok(projects) => projects,
            Err(err) => {
                return JobResult::fail(format!("failed to fetch all projects: {}", err));
            },
        };

        projects
            .into_par_iter()
            .map(|project| {
                // Validate that the webhook is set as expected.
                let webhook_result = webhook_url.map(|webhook_url| {
                    self.verify_webhook(gitlab, host.service.as_ref(), &project, webhook_url)
                });

                // Ensure that the global user is a member of each project.
                let membership_result = global_user.map(|(user, access_level)| {
                    if let Err(err) =
                        host.service
                            .add_member(&project.path_with_namespace, user, *access_level)
                    {
                        JobResult::fail(format!(
                            "failed to add {} to {}: {}",
                            user.handle, project.path_with_namespace, err,
                        ))
                    } else {
                        JobResult::Accept
                    }
                });

                let all_results = vec![webhook_result, membership_result];

                all_results
                    .into_par_iter()
                    .flatten()
                    .reduce(JobResult::accept, JobResult::combine)
            })
            .reduce(
                || JobResult::reject("no projects found"),
                JobResult::combine,
            )
    }

    fn verify_webhook(
        &self,
        gitlab: &Gitlab,
        service: &dyn DirectorHostingService,
        project: &Project,
        webhook_url: &str,
    ) -> JobResult {
        let endpoint = api::projects::hooks::Hooks::builder()
            .project(project.id)
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        let hooks: Vec<Hook> = match endpoint.query(gitlab) {
            Ok(hooks) => hooks,
            Err(err) => {
                return JobResult::fail(format!(
                    "failed to fetch hooks for {}: {}",
                    project.path_with_namespace, err,
                ));
            },
        };

        // Find the hook we're looking to validate.
        if let Some(target_hook) = hooks.into_iter().find(|hook| hook.url == webhook_url) {
            // Validate.
            let expected_hooks = [
                target_hook.issues_events,
                target_hook.job_events,
                target_hook.merge_requests_events,
                target_hook.note_events,
                target_hook.pipeline_events,
                target_hook.push_events,
            ];

            if expected_hooks.iter().all(|&x| x) {
                JobResult::Accept
            } else {
                let endpoint = api::projects::hooks::EditHook::builder()
                    .project(project.id)
                    .hook_id(target_hook.id)
                    .issues_events(true)
                    .job_events(true)
                    .merge_requests_events(true)
                    .note_events(true)
                    .pipeline_events(true)
                    .push_events(true)
                    .build()
                    .unwrap();
                let endpoint = api::ignore(endpoint);
                if let Err(err) = endpoint.query(gitlab) {
                    JobResult::fail(format!(
                        "failed to edit the hook {} on {}: {}",
                        target_hook.id, project.path_with_namespace, err,
                    ))
                } else {
                    JobResult::Accept
                }
            }
        } else {
            // Create the hook.
            if let Err(err) = service.add_hook(&project.path_with_namespace, webhook_url) {
                JobResult::fail(format!(
                    "failed to create the hook on {}: {}",
                    project.path_with_namespace, err,
                ))
            } else {
                JobResult::Accept
            }
        }
    }
}
