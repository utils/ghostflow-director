// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::btree_map::BTreeMap;
use std::collections::btree_set::BTreeSet;
use std::collections::hash_map::{HashMap, Keys};
use std::collections::hash_set::HashSet;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex, MutexGuard, RwLock};
use std::time::{Duration, Instant};

use chrono::Utc;
use either::Either;
use ghostflow::actions::{check, clone, dashboard, data, follow, merge, reformat, stage, test};
use ghostflow::host::{HostedProject, HostingServiceError, MergeRequest, User};
use git_topic_stage::Stager;
use git_workarea::{CommitId, GitContext, GitError, Identity};
use json_job_dispatch::Handler;
use lazy_init::LazyTransform;
use log::{error, warn};
use rayon::prelude::*;
use thiserror::Error;
use topological_sort::TopologicalSort;
use ttl_cache::TtlValue;

use crate::actions::merge_requests::Info;
use crate::config::checks::{CheckError, Checks};
use crate::config::io;
use crate::config::merge_policy;
use crate::ghostflow_ext::{self, DirectorHostingService};
use crate::handlers::{HostError, HostHandler};

pub use crate::config::io::StageUpdatePolicy;

/// Errors which may occur during configuration parsing.
#[derive(Debug, Error)]
pub enum ConfigError {
    /// Failure to parse a configuration file.
    #[error("failed to parse configuration: {}", source)]
    Parse {
        /// The parser error.
        #[from]
        source: io::ConfigError,
    },
    /// Failure to parse a secrets configuration file.
    #[error("failed to parse secrets: {}", source)]
    Secrets {
        /// The parser error.
        #[from]
        source: io::SecretsError,
    },
    /// Failure to construct a host instance.
    #[error("failed to construct a host: {}", source)]
    Host {
        /// The host construction error.
        #[from]
        source: HostError,
    },
    /// A branch is listed as the `into` branch for another, but is not handled on its own.
    #[error("unconfigured into branch: {}", branch)]
    UnconfiguredIntoBranch {
        /// The branch marked as `into` from another that is not configured.
        branch: String,
    },
    /// No maintainers for a host were specified.
    #[error("no maintainers were specified")]
    NoMaintainers {},
    /// Failed to set the default branch name.
    #[error("failed to set default branch name: {:?}: {}", code, output)]
    SetDefaultBranchName {
        /// The exit code of the command.
        code: Option<i32>,
        /// The output of the command.
        output: String,
    },
    /// Failed to set the default branch name.
    #[error("failed to unset default branch name: {:?}: {}", code, output)]
    UnsetDefaultBranchName {
        /// The exit code of the command.
        code: Option<i32>,
        /// The output of the command.
        output: String,
    },
    /// Failure to get project members.
    #[error("failed to get project members: {}", source)]
    GetProjectMembers {
        /// The error from the hosting service.
        #[source]
        source: HostingServiceError,
    },
    /// Failure to clone a project.
    #[error("failed to clone project: {}", source)]
    CloneProject {
        /// The error from the `clone` action.
        #[source]
        source: clone::CloneError,
    },
    /// A `data` action was configured, but with no destinations for the data.
    #[error("there must be at least one destination for syncing data")]
    NoDataDestinations {},
    /// The configured branch's `into` settings form a cycle.
    #[error("circular references involving {} branches in {}", size, name)]
    CircularIntoBranches {
        /// The size of the cycle.
        size: usize,
        /// The name of the project with the cycle.
        name: String,
    },
    /// A branch has conflicting test configurations.
    #[error(
        "at most one of `test` and `tests` may be specified for {}@{}",
        branch,
        project
    )]
    ConflictingTestBackends {
        /// The project with the conflicting test configuration.
        project: String,
        /// The branch with the conflicting test configuration.
        branch: String,
    },
    /// Failure to construct a check.
    #[error("failed to construct checks: {}", source)]
    CheckConfig {
        /// The error from the check.
        #[from]
        source: CheckError,
    },
    /// Failure to add a `check` command.
    #[error("failed to add a check command: {}", source)]
    AddCheck {
        /// The error from the check command.
        #[from]
        source: check::CheckError,
    },
    /// Failure to add a `merge` command.
    #[error("failed to add a merge command: {}", source)]
    AddMerge {
        /// The error from the merge command.
        #[from]
        source: merge::MergeError,
    },
    /// Failure to add set up a merge policy.
    #[error("failed to set up merge policy: {}", source)]
    MergePolicyError {
        /// The error from the merge command.
        #[from]
        source: merge_policy::WorkflowMergePolicyError,
    },
    /// Failure to add a `reformat` command.
    #[error("failed to add a reformat command: {}", source)]
    AddReformat {
        /// The error from the reformat command.
        #[from]
        source: reformat::ReformatError,
    },
    /// Failure to add a `stage` command.
    #[error("failed to add a stage command: {}", source)]
    AddStage {
        /// The error from the stage command.
        #[from]
        source: stage::StageError,
    },
    /// Failure to add a `test` command.
    #[error("failed to add a test command: {}", source)]
    AddTest {
        /// The error from the test command.
        #[from]
        source: TestError,
    },
    /// Failure to run a `git` command.
    #[error("git error: {}", source)]
    Git {
        /// The error from `git`.
        #[from]
        source: GitError,
    },
    /// An empty list of projects is not supported.
    #[error("failed to find any projects")]
    NoProjects {},
    /// Failure to list refs in a project.
    #[error("failed to find the global user: {}", source)]
    UnknownGlobalUser {
        /// The error from the hosting service.
        #[source]
        source: HostingServiceError,
    },
    /// Failure to add set up a merge policy.
    #[error("failed to add service user to project: {}", source)]
    AddUserToProject {
        /// The error from the membership addition.
        #[source]
        source: HostingServiceError,
    },
}

impl ConfigError {
    fn unconfigured_into_branch(branch: String) -> Self {
        ConfigError::UnconfiguredIntoBranch {
            branch,
        }
    }

    fn no_maintainers() -> Self {
        ConfigError::NoMaintainers {}
    }

    fn set_default_branch_name(code: Option<i32>, output: &[u8]) -> Self {
        ConfigError::SetDefaultBranchName {
            code,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn unset_default_branch_name(code: Option<i32>, output: &[u8]) -> Self {
        ConfigError::UnsetDefaultBranchName {
            code,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn get_project_members(source: HostingServiceError) -> Self {
        ConfigError::GetProjectMembers {
            source,
        }
    }

    fn clone_project(source: clone::CloneError) -> Self {
        ConfigError::CloneProject {
            source,
        }
    }

    fn no_data_destinations() -> Self {
        ConfigError::NoDataDestinations {}
    }

    fn circular_into_branches(size: usize, name: String) -> Self {
        ConfigError::CircularIntoBranches {
            size,
            name,
        }
    }

    fn conflicting_test_backends(project: String, branch: String) -> Self {
        ConfigError::ConflictingTestBackends {
            project,
            branch,
        }
    }

    fn no_projects() -> Self {
        Self::NoProjects {}
    }

    fn unknown_global_user(source: HostingServiceError) -> Self {
        Self::UnknownGlobalUser {
            source,
        }
    }
}

type ConfigResult<T> = Result<T, ConfigError>;

/// Errors which may occur when constructing a `test` command.
#[derive(Debug, Error)]
pub enum TestError {
    /// Failure to deserialize a configuration for a backend.
    #[error("test {} config deserialization: {}", backend, source)]
    Deserialize {
        /// The backend in use.
        backend: &'static str,
        /// The deserialization error.
        #[source]
        source: serde_json::Error,
    },
    /// Failure to construct a `jobs` backend.
    #[error("test jobs error: {}", source)]
    Jobs {
        /// The error from the jobs test backend.
        #[from]
        source: test::jobs::TestJobsError,
    },
    /// The service does not support pipelines.
    #[error("the pipelines backend is not supported for this service")]
    NoPipelinesSupport,
}

impl TestError {
    fn deserialize(backend: &'static str, source: serde_json::Error) -> Self {
        TestError::Deserialize {
            backend,
            source,
        }
    }

    fn no_pipelines_support() -> Self {
        TestError::NoPipelinesSupport
    }
}

/// Main configuration object for the robot.
pub struct Config {
    /// The archive directory.
    pub archive_dir: PathBuf,
    /// The directory to watch for new jobs.
    pub queue_dir: PathBuf,
    /// Handler configuration.
    pub handlers: Vec<Box<dyn Handler>>,
}

impl Config {
    /// Read the configuration from a path.
    ///
    /// The `connect_to_host` function constructs `HostingService` instances given an API and a URL
    /// to communicate with.
    pub fn from_path<P>(path: P) -> ConfigResult<Self>
    where
        P: AsRef<Path>,
    {
        Self::from_path_impl(path.as_ref())
    }

    fn from_path_impl(path: &Path) -> ConfigResult<Self> {
        let config = io::Config::from_path(path)?;

        let archive_dir = config.archive_dir().to_path_buf();
        let queue_dir = config.queue_dir().to_path_buf();
        let workdir = PathBuf::from(config.workdir);
        let handlers = config
            .hosts
            .into_iter()
            .map(|(name, host)| {
                let host_workdir = workdir.join(&name);
                let secrets = host.secrets()?;
                let HostHandler {
                    service,
                    handler,
                } = HostHandler::new(&host.host_api, &host.host_url, &secrets, name)?;
                let host = Host::new(host, service, host_workdir)?;

                Ok(handler(host))
            })
            .collect::<ConfigResult<Vec<_>>>()?;

        Ok(Config {
            archive_dir,
            queue_dir,
            handlers,
        })
    }
}

type ProjectInitData = (String, io::Project);
type ProjectInit = LazyTransform<ProjectInitData, Either<Project, ProjectInitData>>;

/// Configuration for a host.
pub struct Host {
    /// The API used to communicate with the host.
    pub api: String,
    /// The `DirectorHostingService` instance to communicate with the host.
    pub service: Arc<dyn DirectorHostingService>,
    /// The users to notify when errors occur with the workflow.
    pub maintainers: Vec<String>,
    /// Projects which have workflow actions configured.
    projects: HashMap<String, ProjectInit>,
    /// A directory which may be used for on-disk scratchspace.
    pub workdir: PathBuf,
    /// The webhook URL to register for new projects.
    pub webhook_url: Option<String>,
    /// A user which should be added to all projects.
    pub global_user: Option<(User, ghostflow_ext::AccessLevel)>,
    /// Whether to unprotect source branches of merge requests or not.
    unprotect_source_branches: bool,
}

/// The possible states for a project.
pub enum ProjectStatus<'a> {
    /// The project has been initialized.
    Initialized(&'a Project),
    /// The project is known, but failed to initialize.
    ///
    /// This usually happens if the clone fails for some reason.
    FailedToInitialize,
    /// The project is unknown.
    Unknown,
}

/// An iterator over project names.
pub type ProjectNames<'a> = Keys<'a, String, ProjectInit>;

impl Host {
    /// Create a new host object from the configuration block.
    pub fn new(
        host: io::Host,
        service: Arc<dyn DirectorHostingService>,
        workdir: PathBuf,
    ) -> Result<Self, ConfigError> {
        if host.maintainers.is_empty() {
            return Err(ConfigError::no_maintainers());
        }

        let projects = host
            .projects
            .into_par_iter()
            .map(|(name, project)| (name.clone(), ProjectInit::new((name, project))))
            .collect::<HashMap<_, _>>();

        let global_user = if let Some(user) = host.global_user.as_ref() {
            let first_project = projects
                .keys()
                .next()
                .ok_or_else(ConfigError::no_projects)?;
            let access_level = user.access_level.into();
            let user = service
                .user(first_project, &user.username)
                .map_err(ConfigError::unknown_global_user)?;

            Some((user, access_level))
        } else {
            None
        };

        Ok(Host {
            api: host.host_api,
            service,
            maintainers: host.maintainers,
            projects,
            workdir,
            webhook_url: host.webhook_url,
            global_user,
            unprotect_source_branches: host.unprotect_source_branches,
        })
    }

    /// Get a project by name.
    ///
    /// Returns `None` if the project doesn't exist or it failed to initialize.
    pub fn project<N>(&self, name: N) -> ProjectStatus
    where
        N: AsRef<str>,
    {
        self.project_impl(name.as_ref())
    }

    /// The implementation of getting a project by name.
    fn project_impl(&self, name: &str) -> ProjectStatus {
        self.projects
            .get(name)
            .map(|init| {
                let project = init.get_or_create(|(name, project)| {
                    let res = Project::new(
                        &name,
                        project.clone(),
                        Arc::clone(&self.service),
                        &self.workdir.join("projects"),
                        &self.maintainers,
                        self.unprotect_source_branches,
                    )
                    .map_err(|err| {
                        error!(
                            target: "ghostflow-director/init",
                            "failed to initialize the {} project: {:?}",
                            name, err,
                        );
                    });

                    match res {
                        Ok(proj) => Either::Left(proj),
                        Err(_) => Either::Right((name, project)),
                    }
                });

                match project {
                    Either::Left(proj) => ProjectStatus::Initialized(proj),
                    Either::Right(_) => ProjectStatus::FailedToInitialize,
                }
            })
            .unwrap_or(ProjectStatus::Unknown)
    }

    /// The names of available projects.
    pub fn project_names(&self) -> ProjectNames {
        self.projects.keys()
    }

    /// Reset project initializations which have failed.
    pub fn reset_failed_projects(&mut self) {
        for (_, value) in self.projects.iter_mut() {
            let data = if let Some(Either::Right(data)) = value.get() {
                warn!(
                    target: "ghostflow-director/init",
                    "resetting initialization of {}",
                    data.0,
                );
                data.clone()
            } else {
                continue;
            };

            *value = ProjectInit::new(data);
        }
    }
}

const MEMBERSHIP_LOCK_POISONED: &str = "membership cache lock poisoned";
const MEMBERSHIP_EXPIRATION: Duration = Duration::from_secs(60 * 60);

type MembershipMap = HashMap<String, ghostflow_ext::Membership>;
type TtlMembershipMap = TtlValue<MembershipMap>;

/// Configuration for a project.
pub struct Project {
    /// The `DirectorHostingService` instance to communicate with the host.
    pub service: Arc<dyn DirectorHostingService>,
    /// A context for working with the git repository of the project.
    pub context: GitContext,
    /// Branches which have workflow actions configured.
    pub branches: HashMap<String, Branch>,
    /// Data handling for the project.
    pub data: Option<data::Data>,
    /// A list of all branches in the project.
    pub branch_names: Vec<String>,
    /// The name of the project.
    name: String,
    /// Users who have non-anonymous access to the project.
    members: RwLock<TtlMembershipMap>,
    /// Users who should be notified on important events.
    maintainers: Vec<String>,
    /// The base name for checks performed on the project.
    base_check_name: String,
    /// The dashboard status actions for the project's commit CI.
    commit_dashboards: Vec<dashboard::Dashboard>,
    /// The dashboard status actions for the project's MR CI.
    mr_dashboards: Vec<dashboard::Dashboard>,
    /// Whether source branches for merge requests should be unprotected or not.
    unprotect_source_branches: bool,
}

impl Project {
    /// Create a new project object from the configuration block.
    fn new(
        name: &str,
        project: io::Project,
        service: Arc<dyn DirectorHostingService>,
        workdir: &Path,
        admins: &[String],
        host_unprotect: bool,
    ) -> ConfigResult<Self> {
        let hosted_project = HostedProject {
            name: name.into(),
            service: service.clone().as_hosting_service(),
        };
        let members = Self::member_map(
            service
                .members(name)
                .map_err(ConfigError::get_project_members)?,
        );
        let mut clone = clone::Clone_::new(workdir, hosted_project.clone());
        project.submodules.iter().for_each(|(name, path)| {
            let link = clone::CloneSubmoduleLink::new(path);
            clone.with_submodule(name, link);
        });
        let context = clone
            .clone_watched_repo()
            .map_err(ConfigError::clone_project)?;
        if let Some(primary_branch) = project.primary_branch {
            let config = context
                .git()
                .arg("config")
                .arg("init.defaultBranchName")
                .arg(primary_branch)
                .output()
                .map_err(|err| GitError::subcommand("config init.defaultBranchName", err))?;
            if !config.status.success() {
                return Err(ConfigError::set_default_branch_name(
                    config.status.code(),
                    &config.stderr,
                ));
            }
        } else {
            let config = context
                .git()
                .arg("config")
                .arg("--unset")
                .arg("init.defaultBranchName")
                .output()
                .map_err(|err| {
                    GitError::subcommand("config --unset init.defaultBranchName", err)
                })?;
            let status = config.status;
            if !status.success() {
                if let Some(5) = status.code() {
                    // Nothing to do; the value didn't exist.
                } else {
                    return Err(ConfigError::unset_default_branch_name(
                        status.code(),
                        &config.stderr,
                    ));
                }
            }
        }
        let identity = {
            let user = service.service_user();
            Identity::new(&user.name, &user.email)
        };
        let project_checks = project.checks.clone();
        let project_pre_checks = project.pre_checks.clone();
        let maintainers = project
            .maintainers
            .iter()
            .cloned()
            .chain(admins.iter().cloned())
            .collect();
        let data = if let Some(data_conf) = project.data {
            let mut data = data::Data::new(context.clone());

            if data_conf.destinations.is_empty() {
                return Err(ConfigError::no_data_destinations());
            }

            data_conf
                .destinations
                .iter()
                .fold(&mut data, data::Data::add_destination);

            if data_conf.keep_refs {
                data.keep_refs();
            }

            data.ref_namespace(data_conf.namespace);

            Some(data)
        } else {
            None
        };

        let into_branches = {
            let mut sorter = TopologicalSort::<&String>::new();
            let direct_into_branches = project
                .branches
                .iter()
                .map(|(branch_name, branch)| {
                    for target_branch in &branch.into_branches {
                        sorter.add_dependency(target_branch, branch_name);
                    }
                    (branch_name, &branch.into_branches)
                })
                .collect::<HashMap<_, _>>();

            let mut into_branches = HashMap::<_, Vec<merge::IntoBranch>>::new();
            while let Some(target_branch) = sorter.pop() {
                let intos = direct_into_branches
                    .get(&target_branch)
                    .ok_or_else(|| ConfigError::unconfigured_into_branch(target_branch.clone()))?
                    .iter()
                    .map(|branch| {
                        let mut into_branch = merge::IntoBranch::new(branch);

                        if let Some(chain_config) = into_branches.get(branch) {
                            into_branch.chain_into(chain_config.iter().cloned());
                        }

                        into_branch
                    })
                    .collect::<Vec<_>>();

                into_branches.insert(target_branch.clone(), intos);
            }

            if !sorter.is_empty() {
                return Err(ConfigError::circular_into_branches(
                    sorter.len(),
                    name.into(),
                ));
            }

            into_branches
        };

        let no_into_branches = Vec::new();
        let branches = project
            .branches
            .into_par_iter()
            .map(|(branch_name, branch)| {
                let mut pre_checks = Checks::new();
                let mut checks = Checks::new();
                let mut seen = HashSet::new();

                branch
                    .checks
                    .iter()
                    .chain(project_checks.iter())
                    .map(|(name, check_config)| {
                        if seen.insert(name) {
                            if let Some(ref kind) = check_config.kind {
                                checks.add_check(kind, check_config.config.clone())
                            } else {
                                Ok(())
                            }
                        } else {
                            Ok(())
                        }
                    })
                    .collect::<Result<Vec<_>, CheckError>>()?;

                if checks.is_empty() {
                    warn!(
                        target: "ghostflow-director/checks",
                        "no checks configured for a branch",
                    );
                }

                if branch.test.is_some() && branch.tests.is_some() {
                    return Err(ConfigError::conflicting_test_backends(
                        name.into(),
                        branch_name,
                    ));
                }

                branch
                    .pre_checks
                    .iter()
                    .chain(project_pre_checks.iter())
                    .map(|check_config| {
                        pre_checks.add_check(&check_config.kind, check_config.config.clone())
                    })
                    .collect::<Result<Vec<_>, CheckError>>()?;

                let mut cbranch = Branch::new(
                    &branch_name,
                    branch.wait_for_pipeline,
                    pre_checks,
                    checks,
                    context.clone(),
                    identity.clone(),
                );

                cbranch.add_issue_labels(branch.issue_labels.iter());
                cbranch.remove_issue_labels(branch.stale_issue_labels.iter());

                if let Some(ref follow) = branch.follow {
                    cbranch.add_follow(follow);
                }

                if let Some(ref merge) = branch.merge {
                    let current_into_branches = into_branches
                        .get(&branch_name)
                        .unwrap_or(&no_into_branches)
                        .clone();
                    cbranch.add_merge(merge, hosted_project.clone(), current_into_branches)?;
                }

                if let Some(ref reformat) = branch.reformat {
                    cbranch.add_reformat(reformat, hosted_project.clone())?;
                }

                if let Some(stage) = branch.stage {
                    cbranch.add_stage(stage, hosted_project.clone())?;
                }

                if let Some(ref test) = branch.test {
                    cbranch.add_test(test, hosted_project.clone())?;
                }

                if let Some(tests) = branch.tests {
                    let access = tests.required_access_level;

                    for (name, test) in tests.backends {
                        let test = test.with_access(access);
                        cbranch.add_test_backend(name, &test, hosted_project.clone())?;
                    }
                }

                Ok((branch_name, cbranch))
            })
            // Not using .collect::<Result<Vec<_>, _>> here to ensure that the error is
            // deterministic.
            .collect::<Vec<_>>()
            .into_iter()
            .collect::<ConfigResult<HashMap<_, _>>>()?;

        let branch_names = branches.keys().cloned().collect();
        let commit_dashboards = project
            .commit_dashboards
            .into_iter()
            .map(|dashboard| {
                dashboard::Dashboard::new(
                    service.clone().as_hosting_service(),
                    dashboard.status_name,
                    dashboard.url,
                    dashboard.description,
                )
            })
            .collect();
        let mr_dashboards = project
            .merge_request_dashboards
            .into_iter()
            .map(|dashboard| {
                dashboard::Dashboard::new(
                    service.clone().as_hosting_service(),
                    dashboard.status_name,
                    dashboard.url,
                    dashboard.description,
                )
            })
            .collect();

        let duration = MEMBERSHIP_EXPIRATION;
        Ok(Project {
            service,
            context,
            branches,
            data,
            branch_names,
            commit_dashboards,
            mr_dashboards,
            name: name.into(),
            members: RwLock::new(TtlValue::new_duration(members, duration)),
            maintainers,
            base_check_name: project.base_check_name,
            unprotect_source_branches: project.unprotect_source_branches.unwrap_or(host_unprotect),
        })
    }

    /// The name of the project.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Add a member to the project.
    ///
    /// Overwrites the previous membership structure.
    pub fn add_member(&self, member: ghostflow_ext::Membership) -> Result<(), HostingServiceError> {
        let mut lock = self.members.write().expect(MEMBERSHIP_LOCK_POISONED);

        if let Some(members) = lock.as_mut() {
            members.insert(member.user.handle.clone(), member);
            return Ok(());
        }

        // Our map has timed out. Instead, just refresh the whole thing.
        self.refresh_membership_impl(&mut lock)?;

        Ok(())
    }

    /// Remove a member from the project.
    pub fn remove_member(&self, user: &User) {
        let mut lock = self.members.write().expect(MEMBERSHIP_LOCK_POISONED);

        if let Some(members) = lock.as_mut() {
            (*members).remove(&user.handle);
        } else {
            // No need to refresh; we're only removing a user. Anything that needs an access level
            // will refresh the map on its own.
        }
    }

    /// Get the access level of a user to the project.
    pub fn access_level(
        &self,
        user: &User,
    ) -> Result<ghostflow_ext::AccessLevel, HostingServiceError> {
        let extract_member = |members: &MembershipMap| {
            members
                .get(&user.handle)
                .and_then(|member| {
                    // If the user's membership has expired, ignore their access level.
                    if let Some(expiration) = member.expiration {
                        if expiration <= Utc::now() {
                            return None;
                        }
                    }

                    Some(member.access_level)
                })
                .unwrap_or(ghostflow_ext::AccessLevel::Contributor)
        };

        {
            let lock = self.members.read().expect(MEMBERSHIP_LOCK_POISONED);

            if let Some(members) = lock.as_ref() {
                return Ok(extract_member(members));
            }
        }

        let mut lock = self.members.write().expect(MEMBERSHIP_LOCK_POISONED);

        // Multiple readers may have detected this state and been queued to refresh the state at
        // the same time; check to see if it is valid again.
        if let Some(members) = lock.as_ref() {
            return Ok(extract_member(members));
        }

        Ok(extract_member(self.refresh_membership_impl(&mut lock)?))
    }

    /// Fetch membership information from the host.
    pub fn refresh_membership(&self) -> Result<(), HostingServiceError> {
        let mut lock = self.members.write().expect(MEMBERSHIP_LOCK_POISONED);

        self.refresh_membership_impl(&mut lock)?;

        Ok(())
    }

    /// Fetch membership information from the host.
    fn refresh_membership_impl<'a>(
        &self,
        members: &'a mut TtlMembershipMap,
    ) -> Result<&'a mut MembershipMap, HostingServiceError> {
        let members_map = Self::member_map(self.service.members(&self.name)?);
        let duration = MEMBERSHIP_EXPIRATION;

        Ok(members.update_ref(members_map, Instant::now() + duration))
    }

    /// Create a hashmap of user IDs to their membership.
    fn member_map(
        members: Vec<ghostflow_ext::Membership>,
    ) -> HashMap<String, ghostflow_ext::Membership> {
        members
            .into_iter()
            .map(|member| (member.user.handle.clone(), member))
            .collect()
    }

    /// The maintainers of the project.
    pub fn maintainers(&self) -> &[String] {
        &self.maintainers
    }

    /// Get the name for statuses for this project.
    pub fn status_name(&self, branch: &str) -> String {
        check::Check::new(
            self.context.clone(),
            self.service.clone().as_hosting_service(),
            Checks::new().config(),
            &self.maintainers,
        )
        .base_name(self.base_check_name.clone())
        .status_name(branch)
    }

    /// Get the check action for a given branch.
    fn pre_check_action<'a>(&'a self, branch: &'a Branch) -> check::Check<'a> {
        check::Check::new(
            self.context.clone(),
            self.service.clone().as_hosting_service(),
            branch.pre_checks.config(),
            &self.maintainers,
        )
        .post_when(check::PostWhen::Failure)
        .base_name(self.base_check_name.clone())
    }

    /// Get the check action for a given branch.
    fn check_action<'a>(&'a self, branch: &'a Branch) -> check::Check<'a> {
        check::Check::new(
            self.context.clone(),
            self.service.clone().as_hosting_service(),
            branch.checks.config(),
            &self.maintainers,
        )
        .base_name(self.base_check_name.clone())
    }

    /// Check that a merge request is OK.
    pub fn check_mr(
        &self,
        branch: &Branch,
        mr: &MergeRequest,
    ) -> Result<check::CheckStatus, check::CheckError> {
        self.check_action(branch).check_mr(
            format!("mr/{}", mr.id),
            &CommitId::new(&branch.name),
            mr,
        )
    }

    /// Check that a merge request is OK with a given commit.
    pub fn check_mr_with(
        &self,
        branch: &Branch,
        mr: &MergeRequest,
        commit: &CommitId,
    ) -> Result<check::CheckStatus, check::CheckError> {
        // Find all target branches for the merge request.
        let backports = Info::backports_for(mr);
        let all_target_branches: BTreeSet<_> = backports
            .iter()
            .filter_map(|bp| {
                // Ignore the current branch.
                if bp.branch() == branch.name {
                    None
                } else {
                    Some(bp.branch())
                }
            })
            .collect();

        // Discover which branches this target branch eventually lands in and exclude them from
        // checks. Because they have already been merged into the target branch(es) that will
        // receive this MR, skip the commits which have already been merged into them.
        let bases = if all_target_branches.is_empty() {
            Vec::new()
        } else {
            // Find all `into` branches for a given branch. Use a closure so that `?` works for
            // `Option` inside of the scope.
            let into_branches_for = |branch: &str| -> Option<BTreeSet<&str>> {
                // Find the branch information in the project.
                let branch = self.branches.get(branch)?;
                // Get its `merge` action. Ignore the `merge_ff` action as it only exists if
                // `merge` exists and uses the same settings anyways.
                let merge = branch.merge.as_ref()?;
                // Collect all `into_branches`. This is a complete closure due to the construction
                // in `Project::new` where the `TopologicalSort` verification builds this as a side
                // effect.
                let mut into_branches: Vec<_> =
                    merge.merge().settings().into_branches().iter().collect();
                // Traverse the graph and collect all unique names.
                let mut all_into_branches = BTreeSet::new();
                while let Some(into) = into_branches.pop() {
                    all_into_branches.insert(into.name());
                    into_branches.extend(into.chain_branches());
                }
                Some(all_into_branches)
            };

            // Only use branches which are being explicitly backported to and are also in the
            // closure of target branches as bases. The intent is to only ignore commits which have
            // already been merged into an `into` branch that are also explicitly participating in
            // the merge request.
            let all_into_branches = into_branches_for(&branch.name).unwrap_or_default();
            all_target_branches
                .intersection(&all_into_branches)
                .copied()
                .map(CommitId::new)
                .collect()
        };

        let pre_check = self.pre_check_action(branch).check_mr_with_bases(
            format!("mr/{}", mr.id),
            &CommitId::new(&branch.name),
            bases.iter(),
            mr,
            commit,
        )?;

        if pre_check == check::CheckStatus::Pass {
            self.check_action(branch).check_mr_with_bases(
                format!("mr/{}", mr.id),
                &CommitId::new(&branch.name),
                bases.iter(),
                mr,
                commit,
            )
        } else {
            Ok(pre_check)
        }
    }

    /// Get an action to merge many branches within the project at once.
    pub fn merge_many(&self) -> merge::MergeMany {
        let project = HostedProject {
            name: self.name.clone(),
            service: self.service.clone().as_hosting_service(),
        };
        merge::MergeMany::new(self.context.clone(), project)
    }

    /// Get the dashboard actions for the project's commit CI.
    pub fn commit_dashboards(&self) -> &[dashboard::Dashboard] {
        &self.commit_dashboards
    }

    /// Get the dashboard actions for the project's MR CI.
    pub fn mr_dashboards(&self) -> &[dashboard::Dashboard] {
        &self.mr_dashboards
    }

    /// Whether source branches for merge requests should be unprotected or not.
    pub fn unprotect_source_branches(&self) -> bool {
        self.unprotect_source_branches
    }
}

/// Representation of a check action for a branch.
pub struct CheckAction;

/// Representation of a follow action for a branch.
pub struct FollowAction {
    /// The follow action itself.
    follow: follow::Follow,
}

impl FollowAction {
    /// A reference to the merge workflow action.
    pub fn follow(&self) -> &follow::Follow {
        &self.follow
    }
}

/// Representation of a merge action for a branch.
pub struct MergeAction {
    /// The merge action itself for the default policy.
    merge: merge::Merge<merge_policy::WorkflowMergePolicy>,
    /// Access level requirement to be able to use the merge action for a branch.
    pub access_level: ghostflow_ext::AccessLevel,
    /// Access level needed for the service user to push the resulting merge.
    pub service_access_level: Option<ghostflow_ext::AccessLevel>,
}

impl MergeAction {
    /// A reference to the merge workflow action.
    pub fn merge(&self) -> &merge::Merge<merge_policy::WorkflowMergePolicy> {
        &self.merge
    }
}

/// Representation of a reformat action for a branch.
pub struct ReformatAction {
    /// The reformat action itself.
    reformat: reformat::Reformat,
    /// Access level requirement to be able to use the reformat action for a branch.
    pub access_level: ghostflow_ext::AccessLevel,
}

impl ReformatAction {
    /// A reference to the reformat workflow action.
    pub fn reformat(&self) -> &reformat::Reformat {
        &self.reformat
    }
}

const STAGE_LOCK_POISONED: &str = "stage lock poisoned";

/// Representation of a stage action for a branch.
pub struct StageAction {
    /// The stage action itself.
    stage: Mutex<stage::Stage>,
    /// The policy for updating branches on this stage.
    pub policy: StageUpdatePolicy,
    /// Access level requirement to be able to use the stage action for a branch.
    pub access_level: ghostflow_ext::AccessLevel,
}

impl StageAction {
    /// A mutable reference to the stage workflow action.
    pub fn stage(&self) -> MutexGuard<stage::Stage> {
        self.stage.lock().expect(STAGE_LOCK_POISONED)
    }
}

/// The backend action to use for the branch's testing.
pub enum TestBackend {
    /// The branch uses the job-based test backend.
    Jobs {
        /// The action itself.
        action: test::jobs::TestJobs,
        /// The help string for the job handler.
        help: String,
    },
    /// The branch uses the ref-based test backend.
    Refs(test::refs::TestRefs),
    /// The branch uses the ref-based test backend.
    Pipelines {
        /// The action itself.
        action: test::pipelines::TestPipelines,
        /// The actor to use when triggering the pipeline.
        actor: io::TestPipelinesActor,
    },
}

/// Representation of a test action for a branch.
pub struct TestAction {
    /// The test backend.
    test: TestBackend,
    /// Access level requirement to be able to use the test action for a branch.
    pub access_level: ghostflow_ext::AccessLevel,
    /// Whether the backend should only respond when explicitly requested.
    pub explicit_only: bool,
    /// Whether the submitter can request a test action or not.
    pub allow_submitter: bool,
    /// Whether to trigger the action for branch updates or not.
    pub test_updates: bool,
}

impl TestAction {
    /// A reference to the test workflow action.
    pub fn test(&self) -> &TestBackend {
        &self.test
    }
}

/// Configuration for a branch within a project.
pub struct Branch {
    /// The name of the branch.
    pub name: String,
    /// Issue labels.
    pub issue_labels: Vec<String>,
    /// Stale issue labels.
    pub stale_issue_labels: Vec<String>,
    /// How to wait for a pipeline to appear.
    pub wait_for_pipeline: Option<io::WaitForPipeline>,

    /// The pre-checks for the branch.
    pre_checks: Checks,
    /// The checks for the branch.
    checks: Checks,

    /// The git context for the branch.
    context: GitContext,
    /// The identity to use for actions on the branch.
    identity: Identity,

    /// The check action for the branch.
    check: CheckAction,
    /// The follow action for the branch.
    follow: Option<FollowAction>,
    /// The merge action for the branch.
    merge: Option<MergeAction>,
    /// The fast-forward merge action for the branch.
    merge_ff: Option<MergeAction>,
    /// The reformat action for the branch.
    reformat: Option<ReformatAction>,
    /// The stage action for the branch.
    stage: Option<StageAction>,
    /// The test action for the branch.
    test: Option<TestAction>,
    /// The test actions for the branch.
    tests: BTreeMap<String, TestAction>,
}

impl From<io::AccessLevelIo> for ghostflow_ext::AccessLevel {
    fn from(access: io::AccessLevelIo) -> Self {
        match access {
            io::AccessLevelIo::Contributor => ghostflow_ext::AccessLevel::Contributor,
            io::AccessLevelIo::Reporter => ghostflow_ext::AccessLevel::Reporter,
            io::AccessLevelIo::Developer => ghostflow_ext::AccessLevel::Developer,
            io::AccessLevelIo::Maintainer => ghostflow_ext::AccessLevel::Maintainer,
            io::AccessLevelIo::Owner => ghostflow_ext::AccessLevel::Owner,
        }
    }
}

impl From<io::MergeTopologyIo> for merge::MergeTopology {
    fn from(topology: io::MergeTopologyIo) -> Self {
        match topology {
            io::MergeTopologyIo::NoFastForward => Self::NoFastForward,
            io::MergeTopologyIo::FastForwardIfPossible => Self::FastForwardIfPossible,
            io::MergeTopologyIo::FastForwardOnly => Self::FastForwardOnly,
        }
    }
}

/// Errors arising from adding a merge command.
#[derive(Debug, Error)]
pub enum AddMergeError {
    /// Failure to add a `merge` command.
    #[error("failed to add a merge command: {}", source)]
    AddMerge {
        /// The error from the merge command.
        #[from]
        source: merge::MergeError,
    },
    /// Failure to add set up a merge policy.
    #[error("failed to set up merge policy: {}", source)]
    MergePolicyError {
        /// The error from the merge command.
        #[from]
        source: merge_policy::WorkflowMergePolicyError,
    },
}

impl From<AddMergeError> for ConfigError {
    fn from(add_merge: AddMergeError) -> Self {
        match add_merge {
            AddMergeError::AddMerge {
                source,
            } => source.into(),
            AddMergeError::MergePolicyError {
                source,
            } => source.into(),
        }
    }
}

impl Branch {
    /// Create a new branch with its checks.
    pub fn new(
        name: &str,
        wait_for_pipeline: Option<io::WaitForPipeline>,
        pre_checks: Checks,
        checks: Checks,
        context: GitContext,
        identity: Identity,
    ) -> Self {
        Branch {
            name: name.into(),
            issue_labels: Vec::new(),
            stale_issue_labels: Vec::new(),
            wait_for_pipeline,

            pre_checks,
            checks,

            context,
            identity,

            check: CheckAction,
            follow: None,
            merge: None,
            merge_ff: None,
            reformat: None,
            stage: None,
            test: None,
            tests: BTreeMap::new(),
        }
    }

    /// Add issue labels for the branch.
    fn add_issue_labels<I, L>(&mut self, labels: I) -> &mut Self
    where
        I: Iterator<Item = L>,
        L: Into<String>,
    {
        self.issue_labels.extend(labels.map(|label| label.into()));
        self
    }

    /// Remove issue labels for the branch.
    fn remove_issue_labels<I, L>(&mut self, labels: I) -> &mut Self
    where
        I: Iterator<Item = L>,
        L: Into<String>,
    {
        self.stale_issue_labels
            .extend(labels.map(|label| label.into()));
        self
    }

    /// Add a merge action to the branch.
    pub fn add_follow(&mut self, follow_conf: &io::Follow) -> &mut Self {
        let mut follow = follow::Follow::new(self.context.clone(), &self.name);
        follow.ref_namespace(&follow_conf.ref_namespace);

        self.follow = Some(FollowAction {
            follow,
        });
        self
    }

    /// Add a merge action to the branch.
    pub fn add_merge<I>(
        &mut self,
        merge_conf: &io::Merge,
        project: HostedProject,
        into_branches: I,
    ) -> Result<&mut Self, AddMergeError>
    where
        I: IntoIterator<Item = merge::IntoBranch>,
    {
        let policy = merge_policy::WorkflowMergePolicy::new(
            merge_conf.policy.clone(),
            project.clone(),
            self.name.clone(),
            self.identity.clone(),
            self.context.clone(),
        )?;
        let mut settings = merge::MergeSettings::new(&self.name, policy);

        if merge_conf.quiet {
            settings.quiet();
        }

        settings.log_limit(merge_conf.log_limit);
        settings.add_into_branches(into_branches);
        settings.merge_branch_as(merge_conf.named.as_ref());
        settings.elide_branch_name(merge_conf.elide.unwrap_or_else(|| self.name == "master"));
        settings.merge_topology(merge_conf.merge_topology.into());

        if let Some(required_access_level_ff) = merge_conf.required_access_level_ff {
            // XXX(check): If the settings ever differ by `into_branches`, look at updating
            // `Project::check_mr_with` as necessary to consider the fast-forward state and to
            // query the appropriate merge action as needed.
            let mut ff_settings = settings.clone();
            ff_settings.merge_topology(merge::MergeTopology::FastForwardOnly);
            self.merge_ff = Some(MergeAction {
                merge: merge::Merge::new(self.context.clone(), project.clone(), ff_settings),
                access_level: required_access_level_ff.into(),
                service_access_level: merge_conf.service_access_level.map(Into::into),
            })
        }

        self.merge = Some(MergeAction {
            merge: merge::Merge::new(self.context.clone(), project, settings),
            access_level: merge_conf.required_access_level.into(),
            service_access_level: merge_conf.service_access_level.map(Into::into),
        });

        Ok(self)
    }

    /// Add a reformat action to the branch.
    pub fn add_reformat(
        &mut self,
        reformat_conf: &io::Reformat,
        project: HostedProject,
    ) -> Result<&mut Self, reformat::ReformatError> {
        let mut reformat = reformat::Reformat::new(self.context.clone(), project);

        let formatters = reformat_conf
            .formatters
            .iter()
            .map(|formatter_conf| {
                let mut formatter =
                    reformat::Formatter::new(&formatter_conf.kind, &formatter_conf.formatter)?;
                formatter.add_config_files(formatter_conf.config_files.iter());
                formatter_conf
                    .timeout
                    .map(|timeout| formatter.with_timeout(Duration::from_secs(timeout)));
                Ok(formatter)
            })
            .collect::<Result<Vec<_>, reformat::ReformatError>>()?;
        reformat.add_formatters(formatters);

        let push_as = match reformat_conf.reformat_as {
            io::ReformatAs::Self_ => reformat::PushAs::Self_,
            io::ReformatAs::Author => reformat::PushAs::Author,
        };
        reformat.push_as(push_as);

        self.reformat = Some(ReformatAction {
            reformat,
            access_level: reformat_conf.required_access_level.into(),
        });
        Ok(self)
    }

    /// Add a stage action to the branch.
    pub fn add_stage(
        &mut self,
        stage_conf: io::Stage,
        project: HostedProject,
    ) -> Result<&mut Self, stage::StageError> {
        let branch = format!("refs/stage/{}/head", self.name);
        let stager = Stager::from_branch(
            &self.context,
            CommitId::new(&self.name),
            CommitId::new(branch),
        )?;

        let mut stage = stage::Stage::new(stager, &self.name, project)?;

        if stage_conf.quiet {
            stage.quiet();
        }

        self.stage = Some(StageAction {
            stage: Mutex::new(stage),
            policy: stage_conf.update_policy,
            access_level: stage_conf.required_access_level.into(),
        });
        Ok(self)
    }

    fn test_action(
        &mut self,
        test_conf: &io::Test,
        project: HostedProject,
    ) -> Result<TestAction, TestError> {
        let (test_backend, updates) = match test_conf.backend {
            io::TestBackend::Jobs => {
                let config: io::TestJobsConfig =
                    serde_json::from_value(test_conf.config.clone())
                        .map_err(|err| TestError::deserialize("jobs", err))?;
                let mut action = test::jobs::TestJobs::new(&config.queue, project)?;

                if config.quiet {
                    action.quiet();
                }

                let backend = TestBackend::Jobs {
                    action,
                    help: config.help_string,
                };

                (backend, config.test_updates)
            },
            io::TestBackend::Refs => {
                let config: io::TestRefsConfig =
                    serde_json::from_value(test_conf.config.clone())
                        .map_err(|err| TestError::deserialize("refs", err))?;
                let mut action = test::refs::TestRefs::new(self.context.clone(), project);

                if config.quiet {
                    action.quiet();
                }
                action.ref_namespace(config.namespace);

                (TestBackend::Refs(action), false)
            },
            io::TestBackend::Pipelines => {
                let config: io::TestPipelinesConfig =
                    serde_json::from_value(test_conf.config.clone())
                        .map_err(|err| TestError::deserialize("pipelines", err))?;
                let service = project
                    .service
                    .as_pipeline_service()
                    .ok_or_else(TestError::no_pipelines_support)?;
                let action = test::pipelines::TestPipelines::new(service);
                let backend = TestBackend::Pipelines {
                    action,
                    actor: config.actor,
                };

                (backend, false)
            },
        };

        Ok(TestAction {
            test: test_backend,
            access_level: test_conf.required_access_level.into(),
            explicit_only: test_conf.explicit_only,
            allow_submitter: test_conf.allow_submitter,
            test_updates: updates,
        })
    }

    /// Add a test action to the branch.
    pub fn add_test(
        &mut self,
        test_conf: &io::Test,
        project: HostedProject,
    ) -> Result<&mut Self, TestError> {
        self.test = Some(self.test_action(test_conf, project)?);
        Ok(self)
    }

    /// Add a test backend action to the branch.
    pub fn add_test_backend(
        &mut self,
        name: String,
        test_conf: &io::Test,
        project: HostedProject,
    ) -> Result<&mut Self, TestError> {
        let action = self.test_action(test_conf, project)?;
        self.tests.insert(name, action);
        Ok(self)
    }

    /// Get the help action for the branch.
    pub fn help(&self) -> Option<()> {
        Some(())
    }

    /// Get the check action for the branch.
    pub fn check_action(&self) -> &CheckAction {
        &self.check
    }

    /// Get the check action for the branch.
    ///
    /// Note that this exists purely to be uniform with other actions.
    pub fn check(&self) -> Option<&CheckAction> {
        Some(self.check_action())
    }

    /// Get the follow action for the branch.
    pub fn follow(&self) -> Option<&FollowAction> {
        self.follow.as_ref()
    }

    /// Get the merge action for the branch.
    pub fn merge(&self) -> Option<&MergeAction> {
        self.merge.as_ref()
    }

    /// Get the fast-forward merge action for the branch.
    pub fn merge_ff(&self) -> Option<&MergeAction> {
        self.merge_ff.as_ref()
    }

    /// Get the reformat action for the branch.
    pub fn reformat(&self) -> Option<&ReformatAction> {
        self.reformat.as_ref()
    }

    /// Get the stage action for the branch.
    pub fn stage(&self) -> Option<&StageAction> {
        self.stage.as_ref()
    }

    /// Get the test action for the branch.
    pub fn test(&self) -> Option<&TestAction> {
        self.test.as_ref()
    }

    /// Get the test backend actions for the branch.
    pub fn tests(&self) -> impl Iterator<Item = (&String, &TestAction)> {
        self.tests.iter()
    }
}
